# list: mutables
l1 = [1, 2, 3, 4]
l2 = list()
l2.append('a')
l2.append('b')
l2.append('c')

print(type(l1))
print(l2)

# tuple: inmutables
t1 = (1, 2, 3, 4)
print(t1[2])

t2 = tuple(l2)
print(t2)

t3 = (1, )
print(type(t3))

# work around to modify a tuple
mt = list(t2)
print(mt)
mt.append('d')
mt2 = tuple(mt)
print(mt2)

# set: NO repetidos, SIN orden.
s1 = {1, 2, 3, 4, 1, 6}
print(s1)
s1.add("Soy nuevo")
s1.add(mt2)
print(s1)

s2 = set(t2)
print(s2)

# dict: key-value
d1 = {
    1: "Uno",
    2: "Dos" 
}

print(d1)
print(d1.get(2, "Sin valor"))

d2 = dict()
d2[3] = "Tre"
print(d2)
d2[3] = "Tres"
print(d2)

print(d1[1])

if 2 in d1:  # Si esta en la keys
    print("Esta")
else:
    print("No esta")

if "Uno" in d1.values():
    print("Esta")
else:
    print("No esta")

for x in d1.keys():
    print(x, d1[x])
