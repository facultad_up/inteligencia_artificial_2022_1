import numpy as np

np.random.seed(0)

x = np.random.rand(15)
print(x)
print()

# Generar una matriz de 4 x 4 con valores entre -10 y 10 reales
# [0, 1) ---> [-10, 10)
# x * 20 - 10

nros = np.random.rand(16) * 20 - 10
print(nros)

matriz = nros.reshape(4, 4)
print(matriz)
