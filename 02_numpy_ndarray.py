# ndarray: es un tipo de dato que representa Matrices/Vectores
import math
import numpy as np

vector1 = np.array((1, 2, 3, 4, 5, 6, 7))
print(vector1)

print(vector1 * math.pi)

vector2 = np.array([8, 9, 10, 11, 12, 13, 14])
print(vector1 + vector2)

print(vector1.shape)
print(vector1.ndim)
print(vector1.size)

vector3 = np.arange(1, 17)      # Vector [1, 2, 3, .... , 24, 25]
print(vector3)
print()

matriz1 = vector3.reshape(4, 4)
print(matriz1)
print(matriz1.shape)
print(matriz1.ndim)
print()

vector1[2] = 1000
print(vector1)
print()

matriz1[2, 1] = 1000000        # matriz1[2][1] = 1000
print(matriz1)
print()

print(matriz1.dtype)
print()

matriz1 = matriz1.astype("float64")
print(matriz1.dtype)
print(matriz1)

