# inteligencia_artificial_2022_1

# Versionado de codigo: git 
* Windows: https://git-scm.com/download/win

- Primera vez para traer el repositorio a la PC
```git clone xxxxx```

- Actualizar en la PC
```git pull```


## Windows: 
Prestar atencion en la primera pantalla: TILDAR "Add to path"


# Ejecucion programas python
- Windows/Linux

- Mac (Antes de la version 12.3 salió 15-03-2022)
  python3

# Manejo de paquetes (librerias)
- pip
- conda

## Manejo de ambientes virtuales
- virtualenv   (es un paquete de python)

- Instalar paquete virtualenv:
```pip install virtualenv```

- Crear el entorno virtual:
```virtualenv env```
o
```python -m virtualenv env```

- Activar el entorno virtual

    * Mac/Linux: ```source env/bin/activate```
    * Windows con bash: ```source env/Scripts/activate```
    * Windows con powershell/cmd: ```.\env\Scripts\activate.bat```

```pip install pandas```

- Listar paquetes instalados con su version
```pip freeze```

- Genera el archivo con los paquetes instalados
```pip freeze > requirements.txt```

- Instalar los paquetes definidos en un archivo
```pip install -r requirements.txt```


